# Installation
## As python script
- python 3.6.5 required, 3.7 - 3.8.x recommended
- python poetry required 
- download and unpack
- in the folder with pyproject.toml file run `poetry install`
- run with `poetry run autocopas/ -v2 --makeconfig`
- fill essential paths in dialog

# Usage
General using poetry: `poetry run autocopas/`
> Windows command may require **poetry.exe** instead

```
  1. 2021-03-24 00:00:00, Кукуруза ДДА 225мл:template=483, inputs=[484, 485]
  2. 2021-03-26 00:00:00, Кукуруза ДДА 225мл:template=483, inputs=[495, 496, 497]
  3. 2021-03-29 00:00:00, т.паста Silvio Grandi 95гр:template=19, inputs=[498]
  4. 2021-03-31 00:00:00, Икра баклажанная УКД 800мл:template=413, inputs=[533]
  5. 2021-04-01 00:00:00, т.паста Едовичка 330гр:template=93, inputs=[557, 558, 559, 560, 561, 562, 563]
select items to render by number, comma-separated or 'all' >
```
Input numbers of products for processing. **all** marks all.

Wait while script renders workbooks one by one.
If file already exists it will promt to abort script completely, overwrite, or save file renamed with appended current day and time.
If it promts for file path template, it failed loading it from constants table, input *relatve* (not starting with '/', '\' or "c:\") path. Special variables are marked with '{}'. e.g. : `{y}/{manufacturer}/Кетчуп/`

## Options
**-v2**, **-v3** - increased verbosity of output
**--list-dicts** - output loaded dictionaries and mappings
**--makeconfig** - write a sample config file, promt for essential paths
**--open-config <program.exe>** - open config file with editor (e.g. notepad.exe)
