# -*- coding: utf-8 -*-

from collections import OrderedDict, namedtuple
from time import time

InspectionKey = namedtuple("InspectionKey", ('date', 'product_name', 'manufacturer', 'form'))


def inspection_key_verbose(key):
    return f"[{key.date}] {key.product_name} {key.manufacturer} {key.form}"


def get_comparable_style_value(cell_obj):
    return str(cell_obj.fill.fgColor.rgb)


class ExcelProccessorMixin:
    master_manufacturers = set()
    header_attr = None  # comparable attribute of product header (e.g. fill foreground colour)

    def cell_is_unchecked(self, cell):
        """ """
        return cell.fill.patternType is None

    def get_rows_to_process(self, ws=None, safety_limit=4000, prepare_checks=True):
        """ read worksheet rows, verify checked rows, return a list of row numbers """
        ws = ws or self.data_worksheet
        counter = 0
        row_numbers = []

        timer = -time()
        while counter < safety_limit:
            row = counter + 1
            cells_data_empty_flags = []
            self.vprint(f"{row: >4}: ", end='', v=3)
            for col_dict in self.config['group_cols']:
                col = col_dict['letter'].upper()
                index = f"{col}{row}"
                cell = ws[index]
                # store list of flags to detect empty data
                cells_data_empty_flags.append(cell.value is None)
                pad = col_dict.get('pad', 20)
                self.vprint(f" | {str(cell.value): <{pad}}", v=3, end=" ")

                if prepare_checks and col_dict.get('role') == 'manufacturer':
                    if row > 1:
                        self.master_manufacturers.add(str(cell.value).strip())

            # skip table head
            if row == 1:
                self.vprint("| (table head)", v=3)
                counter += 1
                continue

            # check cells to select for processing
            do_select = self.cell_is_unchecked(cell)
            clr = "GREEN" if do_select else "YELLOW"
            self.vprint(f"| {do_select}", v=3, colour=clr)

            # check end of data
            if any(cells_data_empty_flags):
                self.vprint(f"* [{row}]: no data in cells, exiting loop", v=2, colour="BLUE")
                break
            elif do_select:
                row_numbers.append(row)
            counter += 1

        timer += time()
        if counter >= safety_limit:
            self.vprint(f"! Reading data table stopped by safety_limit at {counter}", v=1, colour='RED')

        self.vprint(f"* Read rows: {row}, selected for processing: {len(row_numbers)}",
                    v=1,
                    colour='BLUE',
                    end=' ...')
        self.vprint(f" ({timer:.2f} s.)", v=1, colour='green')
        return row_numbers

    def select_rows(self, row_numbers, ws=None):
        """ takes a list of row numbers and returns a dict of (<group_cols>): [{<row_dict>}] """
        ws = ws or self.data_worksheet

        group_cols = [d['letter'] for d in self.config['group_cols']]

        grouped_dict = OrderedDict()
        for row in row_numbers:
            # make a complex key (wow! such magic!)
            key = InspectionKey(*(ws[f"{col}{row}"].value for col in group_cols))
            # row_dict = {col: ws[f"{col}{row}"].value for col in data_cols}
            if key in grouped_dict:
                grouped_dict[key]['input_rows'].append(row)
            else:
                grouped_dict[key] = {'input_rows': [row]}
        return grouped_dict

    def build_manfucaturer_dict(self, head_rows=1):
        self.vprint("> Try to build dictionary of manufacturers", v=2, colour='blue')
        worksheet_name = self.config['dictionary_sheets'].get('manufacturers')
        if not worksheet_name:
            raise RuntimeError("Manufacturers worksheet name is not declared, can't load dictionary")
        ws = self.dict_workbook[worksheet_name]
        row_counter = max(head_rows, 0) + 1
        value = '__init__'
        mnf_dict = {}

        while value:
            key = str(ws.cell(row=row_counter, column=1).value)
            value = ws.cell(row=row_counter, column=2).value
            if key:
                if key in mnf_dict.keys():
                    self.vprint(f"! Key '{key}' found in manufacturers table several times!", v=0, colour="red")
                else:
                    mnf_dict[key] = value
            row_counter += 1
        self.vprint(f"* Loaded {len(mnf_dict)} manufacturers", v=2, colour='blue')
        return mnf_dict
        # add cross-check

    def cross_check(self, mnf_dict=None):
        mnf_dict = mnf_dict or self.manufacturers_dict
        counter = 0
        if self.master_manufacturers and self.manufacturers_dict:
            self.vprint("* run manufacturers cross-check", v=1, colour='blue')
            for key in self.master_manufacturers:
                if key in self.manufacturers_dict.keys():
                    counter += 1
                else:
                    self.vprint(f' - ! "{key}" is not found in manufacturers dict!',
                                v=1, colour='red')
            self.vprint(f"* {counter} manufacturers are present", v=1, colour='blue')
        else:
            self.vprint("! cross-check skipped!", v=1, colour='yellow')

    def build_product_index(self, safety_limit=3000, empty_limit=40):
        """ reads mapping worksheet and returns dict of lowercased product title - row number """
        sheet_name = self.config['dictionary_sheets'].get('data_mapping')
        self.vprint(
            f"* Build index with data mapping from '{sheet_name}'",
            v=1,
            colour='BLUE'
        )

        if not sheet_name:
            raise RuntimeError("Couldn't get data mapping sheet name from config")
        ws = self.dict_workbook[sheet_name]

        counter = 0
        empty_counter = 0
        index = {}
        while counter < safety_limit and empty_counter <= empty_limit:
            counter += 1
            cell_obj = ws[f'A{counter}']
            if cell_obj.value:
                # important reset empty_counter, it counts empty cells in-between data
                empty_counter = 0

                # pick a self.header_attr (e.g. fgColor.rgb) so it can be compared
                if self.header_attr is None:
                    self.header_attr = get_comparable_style_value(cell_obj)
                if self.header_attr == get_comparable_style_value(cell_obj):
                    if counter == 2:
                        raise RuntimeError(f"Fill attribute ({self.header_attr}) is equal for 2 first rows."
                                           f" First row must set distinct fill pattern for script."
                                           f" Attribute retrieval is done with get_comparable_style_value"
                                           f" method.")
                    msg = f"% attribute ({self.header_attr}) match at #{counter}, '{cell_obj.value}'"
                    self.vprint(msg, v=3, colour="CYAN")
                    titles = str(cell_obj.value.lower()).split('|')
                    for title in titles:
                        normalized_title = title.strip()
                        if normalized_title in index.keys():
                            raise RuntimeError(f" Product '{cell_obj.value}' is already in the index!"
                                            f" Row {counter}, first occurance {index[normalized_title]}")
                        index[normalized_title] = counter
            else:
                empty_counter += 1
                continue

        if empty_counter > empty_limit:
            msg = f"* Stop by empty_counter limit == {empty_limit} at row {counter}"
            self.vprint(msg, v=1, colour='BLUE')

        if counter >= safety_limit:
            msg = f"! Stop by safety_limit == {safety_limit} !!! Some data may be lost!"
            self.vprint(msg, v=0, colour='RED')

        index_size = len(index)
        if index_size > 0:
            msg = f"* built product index of {index_size} items"
            self.vprint(msg, v=1, colour='BLUE')
        else:
            raise RuntimeError(f"Couldn't built any product index, make sure '{sheet_name}'"
                               " was formatted correctly and has data. First row marks fill"
                               " to find product headers. Don't use indexed colours or complex"
                               " fills."
                               )
        return index

    def get_cell_value_or_warn(self, col, row, ws, raise_error=False):
        """ get cell value from constant worksheet or warn if empty """
        value = ws[f"{col}{row}"].value
        if not value:
            msg = f"Cell [{col}{row}] is not filled!"
            self.reader_warnings.append(msg)
            self.vprint(msg, colour='red', v=2)
            if raise_error:
                raise RuntimeError(msg)
            value = None
        else:
            value = value.strip()
        return value

    def build_constants_dict(self, ws=None, safety_limit=1000):
        """ load constants from dict sheet
            returns a dict with composite key of 3-tuple of name, manufacturer, format (1, 2, 3)
        """
        self.vprint(f"* Load constants", v=1, colour='BLUE', end='... ')
        timer = -time()

        ws = ws or self.dict_workbook[self.config['dictionary_sheets']['constants']]
        key_cols = self.config.get('constants_key_col_nums')
        path_col = self.config.get('constants_path_col')
        title_col = self.config.get('constants_title_col')
        name_col = self.config.get('constants_product_name_col')

        composite_key = tuple(True for i in key_cols)
        constants_dict = {}
        counter = 2
        while counter < safety_limit and all(composite_key):
            composite_key = tuple(ws.cell(row=counter, column=i).value for i in key_cols)
            if all(composite_key):
                constants_dict[composite_key] = {
                    'path_template': self.get_cell_value_or_warn(path_col, counter, ws),
                    'title_template': self.get_cell_value_or_warn(title_col, counter, ws),
                    'product_name': self.get_cell_value_or_warn(name_col, counter, ws),
                }
            counter += 1

        if constants_dict:
            if counter < safety_limit:
                status = "OK"
                colour = "green"
                level = 2
            else:
                status = "BREAK"
                colour = "red"
                level = 0
        else:
            status = "EMPTY"
            colour = "red"
            level = 0
        timer += time()
        self.vprint(f"{status} ({timer:.2f} s.)", v=level, colour=colour)
        if status == "BREAK":
            self.vprint(f"! stoped by safety limit at {counter}", v=0, colour="red")
        elif status == "OK":
            self.vprint(F"* loaded {len(constants_dict)} items", v=1, colour='blue')
        return constants_dict

    def match_product_dict_row(self, product_name, form, manufacturer,
                               product_index=None, raise_error=True):
        """ returns number of row of data mapping """
        index_is_external = (product_index is not None) 
        product_index = product_index or self.product_index  # select internal index if not overriden

        if not product_index:
            raise RuntimeError(f"Product_index is empty! ({product_index!r})")

        self.vprint(
            f"* Try to match {product_name} {form} ({manufacturer}) with product index"
            f" ({len(product_index)} items, external: {index_is_external})",
            v=3,
            colour='CYAN'
        )

        # important - exact keys to bottom. Higher index means closer match
        product_keys = (product_name,
                        f"{product_name} {form}",
                        f"{product_name} ({manufacturer})",
                        f"{product_name} {form} ({manufacturer})")
        product_keys = [i.lower() for i in product_keys]

        counter = 0
        empty_counter = 0
        match_candidates = {}

        for key in product_keys:
            if key in product_index:
                match_candidates[counter] = (key, product_index[key])
            counter += 1

        len_candidates = len(match_candidates)
        if len_candidates > 0:
            match_index = max(match_candidates.keys())
            product_template_mapping = match_candidates[match_index]
            self.vprint(f"* mapping selected: {product_template_mapping}", v=2, colour='blue', end=' ')
            if len_candidates > 1:
                # see product_keys declaration
                if match_index == 2 and 1 in match_candidates:
                    # questionable selection
                    self.vprint(f"! selected mapping for '{product_template_mapping[0]}' MAY BE NOT"
                                f" accurate, since there is match for '{product_keys[1]}'"
                                f" (row {product_index[product_keys[1]]})",
                                v=1, colour='yellow')
                else:
                    msg = f"? {len_candidates} tried, best match selected"
                    self.vprint(msg, v=3, colour='cyan')
            self.vprint('', v=2)
        else:
            product_template_mapping = None
            self.vprint(f"! no match found, tried in this order: {'; '.join(product_keys)}."
                        f" (Matching lowercased)",
                        v=1, colour='red')

        return product_template_mapping
