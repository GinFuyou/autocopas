# -*- coding: utf-8 -*-

import re
import sys
import warnings
from copy import copy
from datetime import date, datetime
from pathlib import Path, PureWindowsPath
from time import time

from openpyxl import Workbook
from openpyxl.cell.cell import MergedCell
from openpyxl.utils import get_column_letter

from autocopas.reader import get_comparable_style_value, inspection_key_verbose

OUTPUT_TABLE_START_ROW = 4


class WorkbookWriter(object):
    merging_iterations = 0
    merging_time = 0.0
    col_addr_pattern = re.compile(r"^[A-Z]{1,3}$")
    select_pattern = re.compile(r"^(?P<column>[A-Z]{1,3})\s+выборка")
    cell_addr_pattern = re.compile(r"(([A-Z]{1,3})\d+)")
    formula_pattern = re.compile(r"=(?P<formula>[0-9A-Z\*\-\+\\\/\(\)\s]+)\s?ФОРМУЛА\!?")
    ticker = "....*"
    list_of_empty_value_cells = None
    list_of_saved_files = None

    def __init__(self, template_worksheet, data_worksheet, config):
        self.template_worksheet = template_worksheet
        self.data_worksheet = data_worksheet
        self.output_path = Path(config['dirs']['output_path'])
        self.config = config
        self.date = date.today()
        self.tick_state = 0
        self.list_of_empty_value_cells = []
        self.list_of_saved_files = []

        if not self.output_path.is_dir():
            raise RuntimeError(f"Output path '{self.output_path}' is not an"
                               " existing directory!")

    def tick(self):
        """ visual progress feedback """
        size = len(self.ticker)
        output = " " + self.ticker[0] * self.tick_state
        output += self.ticker[-1]
        output += self.ticker[0] * (size - self.tick_state - 1)
        print(output, end="\r")
        self.tick_state = (self.tick_state + 1) % size

    def find_range_of_template(self, template_row, header_attr):
        cell_obj = self.template_worksheet[f"A{template_row}"]
        style_value = get_comparable_style_value(cell_obj)
        col_counter = 1

        if style_value == header_attr:
            while style_value == header_attr:
                col_counter += 1
                cell_obj = self.template_worksheet[f"{get_column_letter(col_counter)}{template_row}"]
                style_value = get_comparable_style_value(cell_obj)
        else:
            raise RuntimeError(f"Writer got different value for product header markup '{style_value}'"
                               f" (cell A{template_row}) then passed to it {header_attr}."
                               " See get_comparable_style_value() function for details.")
        # DEBUG
        # print(f"column range: {col_counter} [{get_column_letter(col_counter)}]")

        row_counter = 0  # start from 0 because of offset to starting row
        while style_value != header_attr:
            row_counter += 1
            cell_obj = self.template_worksheet[f"A{template_row+row_counter}"]
            style_value = get_comparable_style_value(cell_obj)

        # print(f"row range: {row_counter}")
        return (range(1, col_counter), range(template_row+1, template_row+row_counter))

    def copy_style(self, from_cell, to_cell, attrs=('font', 'border', 'fill', 'number_format', 'alignment')):
        if from_cell.has_style:
            for attr in attrs:
                val = copy(getattr(from_cell, attr))
                setattr(to_cell, attr, val)

    def copy_merge(self, ws, coord, row):
        t = -time()
        for merged_range in self.template_worksheet.merged_cells.ranges:
            if coord in merged_range:
                min_col, max_col = merged_range.min_col, merged_range.max_col
                if merged_range.max_row != merged_range.min_row:
                    warnings.warn(f"!! Merged cell range {str(merged_range)} spans row, which is not implemented!"
                                  " Only columns will be merged in output worksheet!")
                min_row = max_row = row  # NOTE merging rows is not implemented
                ws.merge_cells(start_row=min_row, start_column=min_col, end_row=max_row, end_column=max_col)
                # print(f"merge {get_column_letter(min_col)}{min_row} : {get_column_letter(max_col)}{max_row}")
                break
        t += time()
        self.merging_time += t
        self.merging_iterations += 1

    def match_pattern_as_single_cell_addr(self, clean_value):
        """ identify template cell values as single address e.g. 'AC' """
        if not isinstance(clean_value, str):
            raise RuntimeError(f"Matching template cell patterns received non-string value for"
                               f" input, please str().strip() it first!")
        return self.col_addr_pattern.match(clean_value)

    def match_pattern_as_formula(self, clean_value):
        """ identify template cell values as single address e.g. '=J256/I256*100 ФОРМУЛА!' """
        if not isinstance(clean_value, str):
            raise RuntimeError(f"Matching template cell patterns received non-string value for"
                               f" input, please str().strip() it first!")
        return self.formula_pattern.match(clean_value)

    def match_pattern_as_component_selection(self, clean_value):
        if not isinstance(clean_value, str):
            raise RuntimeError(f"Matching template cell patterns received non-string value for"
                               f" input, please str().strip() it first!")
        return self.select_pattern.match(clean_value)

    def format_title_date(self, date, locale='ru'):
        """ format date for title (A1) of an output worksheet """
        formated_date = date.strftime(self.config['output_header_date_format'])
        if "MMMM" in formated_date:
            monthes = {1: "января",
                       2: "февраля",
                       3: "марта",
                       4: "апреля",
                       5: "мая",
                       6: "июня",
                       7: "июля",
                       8: "августа",
                       9: "сентября",
                       10: "октября",
                       11: "ноября",
                       12: "декабря"}
            m = monthes.get(date.month, "???")
            formated_date = formated_date.replace("MMMM", m)
        return formated_date

    def process_cell(self, cell, input_row, row_counter):
        value = cell.value
        if input_row:
            clean_value = str(value).strip()
            addr = f"{clean_value}{input_row}"
            if self.match_pattern_as_single_cell_addr(clean_value):
                # print(f"{cell.coordinate} [{value}] matches reference pattern")
                value = self.data_worksheet[addr].value
                if not value:
                    self.list_of_empty_value_cells.append(addr)
                return value

            # else
            m = self.match_pattern_as_formula(clean_value)
            if m:
                formula = m.group('formula')
                print(f"DETECTED formula: {formula}")
                addr_iterator = self.cell_addr_pattern.findall(formula)
                for addr, col in addr_iterator:
                    print(f"[{addr}] -> [{col}{row_counter}]")
                    formula = formula.replace(addr, f"{col}{row_counter}")
                value = f"={formula}"
                return value

            # else TODO: expand copying?
            m = self.match_pattern_as_component_selection(clean_value)
            if m:
                column = m.group('column')
                addr = f"{column}{input_row}"
                print(f"DETECTED selection from {addr}")
                value = self.data_worksheet[addr].value
                if not value:
                    self.list_of_empty_value_cells.append(addr)
        return value

    def copy_columns(self, ws, row, row_counter, col_range, input_row=None):
        """ copy range of columns in given row of worksheet"""
        for col in col_range:
            tmpl_coord = f"{get_column_letter(col)}{row}"
            # print(tmpl_coord, end=' ')
            tmpl_cell = self.template_worksheet[tmpl_coord]

            value = self.process_cell(tmpl_cell, input_row, row_counter)
            out_cell = ws.cell(row=row_counter, column=col, value=value)
            self.copy_style(tmpl_cell, out_cell)

            if isinstance(tmpl_cell, MergedCell):
                self.copy_merge(ws, tmpl_coord, row_counter)

        row_height = self.template_worksheet.row_dimensions[row].height
        if row_height:
            ws.row_dimensions[row_counter].height = row_height

    def select_product_constnats(self, product_key, constants_dict):
        """ use product_key to return constants tuple from constants_dict by composite key """
        composite_key = (product_key.product_name,
                         product_key.manufacturer,
                         product_key.form)
        if composite_key in constants_dict:
            path_template = constants_dict[composite_key]['path_template']
            title_template = constants_dict[composite_key]['title_template']
            product_name = constants_dict[composite_key]['product_name']
            return path_template, title_template, product_name
        else:
            return None, None, None

    def render(self, product_group, header_attr, manufacturer_mapping, constants_dict):
        """ render template with data for given product group and dict data """
        product_key, product_inputs = product_group
        template_row = product_inputs['template_mapping']
        if not template_row:
            raise RuntimeError(f"render() method recieved product_group <{product_group}>"
                               f" with empty row address '{template_row}'")

        col_range, row_range = self.find_range_of_template(template_row, header_attr)

        print(f' -- Processing "{inspection_key_verbose(product_key)}" columns {col_range},'
              f" rows {row_range} --")

        # row_offset = row_range[-1] - 2
        wb = Workbook()
        ws = wb.active
        row_counter = OUTPUT_TABLE_START_ROW
        for row in row_range:
            if row_counter == OUTPUT_TABLE_START_ROW + 1:  # template row
                input_row_numbers = product_inputs['input_rows']
                # print(f"\n template row, ({len(input_row_numbers)}): ", end='')
                for input_row in input_row_numbers:
                    self.copy_columns(ws, row, row_counter, col_range, input_row=input_row)
                    row_counter += 1
            else:
                # print("\ncopy: ", end='')
                self.copy_columns(ws, row, row_counter, col_range)
                row_counter += 1
            self.tick()
            # print()

        for col in col_range:
            letter = get_column_letter(col)
            col_width = self.template_worksheet.column_dimensions[letter].width
            if col_width:
                ws.column_dimensions[letter].width = col_width

        if self.list_of_empty_value_cells:
            print(f"! Following {len(self.list_of_empty_value_cells)} cells contained no data!:"
                  f" {', '.join(self.list_of_empty_value_cells)}")
        print(f"Merging cells: {self.merging_iterations} iterations in {self.merging_time:.2f} sec.")
        print(", ".join((str(r) for r in ws.merged_cells.ranges)))
        print(f' -- Finishing "{inspection_key_verbose(product_key)}" -- ')

        # write manufacturer into A3
        verbose_manufacturer = manufacturer_mapping.get(product_key.manufacturer)
        cell = ws.cell(row=3, column=1, value=verbose_manufacturer)
        font = copy(cell.font)
        font.bold = True
        font.italic = True
        cell.font = font
        ws.merge_cells(start_row=3, start_column=1, end_row=3, end_column=self.config.get('merge_cols_in_top_rows', 4))

        path_template, title_template, product_name = self.select_product_constnats(product_key, constants_dict)

        # write title (A1)
        if title_template:
            formated_date = self.format_title_date(product_key.date)
            title = title_template.replace("ДАТА МЕСЯЦ ГОД", formated_date)
            cell = ws.cell(row=1, column=1, value=title)
            cell.font = font
            ws.merge_cells(
                start_row=1,
                start_column=1,
                end_row=1,
                end_column=self.config.get('merge_cols_in_top_rows', 4)
            )  # merge cells in top rows
        else:
            print("! title template is not loaded, fix it later yourself!")

        self.save(wb, product_key, path_template, product_name)

    def save(self, wb, product_key, path_template, product_name):
        if not path_template:
            a = ""
            while not a:
                print('\nPath template was not loaded from constants sheet.'
                      '\nInput it manually now. Don\'t use "" or start with "\\", "/"'
                      '\n- {y} for year'
                      '\n- {m} for month'
                      '\n- {d} for day'
                      '\n- {manufacturer} for manufacturer company name (short)')
                a = input("\n> ").strip()
            path_template = a

        save_path = path_template.format(
            date=self.date,
            y=product_key.date.year,
            m=product_key.date.month,
            d=product_key.date.day,
            manufacturer=product_key.manufacturer)
        if "\\" in save_path:
            save_path = Path(PureWindowsPath(save_path).as_posix())
            # print(f"convert to {save_path}, ({type(save_path)})")

        filename_format = self.config['filename_format'].format(
            key=product_key,
            product_name=product_name or product_key.product_name)

        save_path = self.output_path / save_path / product_key.date.strftime(filename_format)
        if not save_path.parent.exists():
            print(f"> Will create '{save_path.parent}'")
            save_path.parent.mkdir(parents=True, exist_ok=True)
        if save_path.exists():
            a = ""
            while a not in ('A', 'O', 'R'):
                print(f'\nFile "{save_path}" already exists.\n(O) Overwrite (A) Abort (R) Rename and save')
                a = input("> ").strip().upper()
            if a == "A":
                sys.exit(4)
            elif a == "R":
                stem_addition = datetime.now().strftime(" (%j %H:%M:%S)")
                # save_path = save_path.with_stem(save_path.stem + stem_addition)  # NOTE requires newer python
                save_path = save_path.with_name(save_path.stem + stem_addition + save_path.suffix)
            elif a == "O":
                pass
            elif a == "S":
                print('Skipping')
                return
        print(f"Save to '{save_path}'")
        self.list_of_saved_files.append(save_path)
        wb.save(save_path)
