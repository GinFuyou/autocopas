import unittest
from datetime import date

from autocopas.base import CONFIG_VERSION, BaseCommand
from autocopas.writer import WorkbookWriter

# all formulas has to be valid, pattern mismatch will fail the test
FORMULA_EXAMPLES = [
    "=L3*100/(100-N3)\nФОРМУЛА!",
    "=L3*100/(J3+K3) ФОРМУЛА!",
    "=J256/I256*100\nФОРМУЛА!"
]


class TestWriter(unittest.TestCase):
    config = BaseCommand.config

    def setUp(self):
        """ TODO: Add config reading """
        print(f"\nInit {WorkbookWriter.__name__}")
        self.writer = WorkbookWriter(None, None, self.config)

    def test_formula(self):
        self.assertEqual(self.config.get('config_version'), CONFIG_VERSION)
        print(f"Config version: {self.config.get('config_version')}")
        print(f"\n* Will test {len(FORMULA_EXAMPLES)} formula examples:")
        print(f"  (re pattern: '{self.writer.formula_pattern.pattern}')\n")
        counter = 0
        results = []
        for formula in FORMULA_EXAMPLES:
            counter += 1
            print(f" {counter}) '{formula}'", flush=True, end=' ')
            re_match = self.writer.match_pattern_as_formula(formula)
            results.append(re_match)
            print(f"[{bool(re_match)}]")
        for re_match in results:
            self.assertTrue(re_match)

    def test_title_date(self):
        fmt = self.config['output_header_babeldate_format']
        print("\nTest output worksheet title date format"
              f"\nFormat: '{fmt}'")
        test_date = date.today()
        print(self.writer.format_title_date(test_date))

if __name__ == '__main__':
    unittest.main()
