# -*- coding: utf-8 -*-

import pprint
import re
import sys
import traceback
from time import time

from colorama import Back, Fore, Style
from openpyxl import load_workbook

from autocopas.base import BaseCommand
from autocopas.reader import ExcelProccessorMixin
from autocopas.writer import WorkbookWriter


class Command(ExcelProccessorMixin, BaseCommand):
    help = ('Process main data workbook and write output based on template workbook.'
           ' Use --makeconfig at the first run to write a configuration file.'
           ' Use -c to set path to config file (default is system application data dir).'
           ' On Windows OS use -w to see colours (may show gibberish symbols without it).'
           )
    version = '0.72.2-b-2'
    name = 'AutoCopas'

    def read_workbook(self, path, sheet=None, data_only=False):
        """ loads an Excel workbook and returns it or sheet by name """
        msg = f"* Loading workbook '{path}'"
        if sheet:
            msg += f", sheet '{sheet}'"
        if data_only:
            msg += " (Values only)"
        self.vprint(msg, v=2, colour="BLUE", end='...')
        timer = -time()
        try:
            wb = load_workbook(filename=path, data_only=data_only)
        except FileNotFoundError:
            timer += time()
            self.vprint(f" FAILURE [{timer:.2f}]s.", v=0, colour="RED")
            traceback.print_exc()
            self.vprint(f"! File for workbook is not found at '{path}'."
                        f" Please check your config! (use --open-config)",
                        v=0,
                        colour='YELLOW')
            sys.exit(1)
        except PermissionError:
            timer += time()
            self.vprint(f" FAILURE [{timer:.2f}]s.", v=0, colour="RED")
            traceback.print_exc()
            self.vprint(f"! File for workbook at '{path}' couldn't be opened."
                        f" Please check if you have permissions to access it!",
                        v=0,
                        colour='YELLOW')
            sys.exit(2)
        except Exception as ex:
            timer += time()
            self.vprint(f" FAILURE [{timer:.2f}]s.", v=0, colour="RED")
            raise ex
        else:
            timer += time()
            self.vprint(f" OK ({timer:.2f}s.)", v=2, colour="GREEN")

        # if sheet passed as argument, return it, else return whole workbook
        if sheet:
            if sheet in wb.sheetnames:
                ws = wb[sheet]
            else:
                ws = None
                STL = Back.BLUE + Fore.BLACK
                msg = f"! Sheet '{sheet}' not found! options are: {Style.RESET_ALL}" \
                      f"{STL}{f'{Style.RESET_ALL}, {STL}'.join(wb.sheetnames)}" \
                      f"{Style.RESET_ALL}"
                self.vprint(msg, v=0, colour="RED")
            return ws
        else:
            return wb

    def execute(self):
        """ command entry point, use Command().execute() """
        super().execute()

        if self.args.makeconfig:
            self.make_config()
        self.config = self.read_config()

        # main operation block
        self.data_worksheet = self.read_workbook(
            self.config['dirs']['data_table_path'],
            self.DATA_SHEET_NAME,
            data_only=self.config.get('data_table_values_only', True))

        if not self.data_worksheet:
            raise RuntimeError(f"Couldn't load worksheet from"
                               f"'{self.config['dirs']['data_table_path']}'")
            sys.exit(1)

        self.dict_workbook = self.read_workbook(self.config['dirs']['mapping_dict_path'])

        self.manufacturers_dict = self.build_manfucaturer_dict()
        if self.args.list_dicts:
            pprint.pprint(self.manufacturers_dict)

        row_numbers = self.get_rows_to_process()
        if not row_numbers:
            self.vprint(f"! No rows selected for processing, exiting.", v=1, colour='yellow')

        self.cross_check()  # check manufacturers mapping
        self.vprint(f"* Rows numbers selected: {', '.join(str(i) for i in row_numbers)}",
                    v=2,
                    colour='blue')

        self.constant_dict = self.build_constants_dict()
        if self.args.list_dicts:
            pprint.pprint(self.constant_dict)

        grouped_dicts = self.select_rows(row_numbers)
        if self.args.list_dicts:
            pprint.pprint(grouped_dicts)

        self.product_index = self.build_product_index()

        if self.args.list_dicts:
            pprint.pprint(self.product_index)

        for key in grouped_dicts.keys():
            row = self.match_product_dict_row(product_name=key.product_name,
                                              manufacturer=key.manufacturer,
                                              form=key.form)
            grouped_dicts[key]['template_mapping'] = row[1] if row else None

        list_of_groups = []
        i = 0
        self.vprint("  ▼  [use this numbers for input] rows without number are missing data",
                    v=1,
                    colour='magenta')

        for key, value in grouped_dicts.items():
            if all(value.values()):
                i += 1
                list_of_groups.append((key, value))
                print(f"{i: >3}. )", end=' ')
                colour = "white"
            else:
                colour = "yellow"
                self.vprint(" --- )", v=0, colour=colour, end=' ')
            self.vprint(f"{key.date}, {key.product_name} {key.manufacturer} {key.form}:"
                        f" template={value['template_mapping']}, inputs={value['input_rows']}",
                        v=0, colour=colour)

        if self.reader_warnings:
            self.vprint(f"! Warnings issued during reading: {len(self.reader_warnings)}"
                        f" (some data may be invalid or not filled!)"
                        f"\n  use -v2 argument to see them during script run",
                        v=0,
                        colour="yellow")

        self.args.interactive = True  # NOTE if non-interactive mode will be added
        if self.args.interactive:
            a = input("select items to render by number, comma-separated or 'all' > ")
            a = a.lower().strip()
            if a in ("all", "все"):
                indices = range(1, i+1)
            elif a == "":
                self.vprint("No items selected, exit", v=0, colour='yellow')
                exit(3)
            elif re.match(r"^\d{1,3}\s?(?:\s?\,\s?\d{1,3})*$", a):
                indices = [int(i.strip())-1 for i in a.split(',')]

        mapping_sheet_name = self.config['dictionary_sheets'].get('data_mapping')
        self.writer = WorkbookWriter(self.dict_workbook[mapping_sheet_name],
                                     self.data_worksheet, self.config)
        # print(self.writer.find_range_of_template(19, self.header_attr))
        for index in indices:
            self.writer.render(list_of_groups[index],
                               self.header_attr,
                               self.manufacturers_dict,
                               self.constant_dict)
        if len(self.writer.list_of_saved_files) > 1:
            self.vprint("* Following files were saved:", v=1, colour="green")
        for path in self.writer.list_of_saved_files:
            self.vprint(f"{path}", v=1)

        self.vprint("[Finished]", v=1, colour='cyan')


if __name__ == '__main__':
    Command().execute()
