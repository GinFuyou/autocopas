# -*- coding: utf-8 -*-


import argparse
import pprint
import sys
from pathlib import Path
from platform import python_version
from subprocess import run

import appdirs  # narrow down TODO
import yaml
from colorama import AnsiToWin32, Back, Fore, Style
from colorama import init as colorama_init

CONFIG_VERSION = 16


class BaseCommand(object):
    args = None
    start_text = ('* {0.name} ver.{1.MAGENTA}{0.version}{2.RESET_ALL} start!'
                  ' (v={0.args.verbosity}) [python {1.YELLOW}{3}{2.RESET_ALL}]')
    default_config_path = None
    config = {
        "config_version": CONFIG_VERSION,
        "dirs":
            {
                'data_table_path': '',
                'mapping_dict_path': '',
                'output_path': '',
            },
        "data_table_values_only": True,
        "dictionary_sheets":
            {
                "data_mapping": "Константы и столбцы",
                "manufacturers": "Производители",
                "constants": "Константы1",
            },
        "group_cols": [{'letter': 'B', 'role': 'date', 'type': 'date'},
                       {'letter': 'C', 'role': 'name', 'type': ''},
                       {'letter': 'D', 'role': 'manufacturer', 'type': ''},
                       {'letter': 'E', 'role': 'format', 'type': '', 'pad': 6}],
        "merge_cols_in_top_rows": 5,
        "constants_title_col": "D",
        "constants_path_col": "R",
        "constants_product_name_col": "S",
        "constants_key_col_nums": [1, 2, 3],
        "output_header_date_format": "d MMMM y г.",
        "filename_format": "{product_name} {key.form} {key.manufacturer} %d.%m.%y.xlsx"
    }

    DATA_SHEET_NAME = 'инспек.контроль'  # TODO CONFIG

    product_index = None
    writer = None

    # tables
    data_worksheet = None
    dict_workbook = None

    reader_warnings = None

    def __init__(self, config_path=None):
        self.default_config_path = (
            config_path or Path(appdirs.user_config_dir(self.name, "Doratoa")) / 'autocopas.conf'
        )
        self.reader_warnings = []

    def extra_verborse(self):
        return self.args.verbosity > 1

    def run_argparse(self):
        """
        Create and return the ``ArgumentParser`` which will be used to
        parse the arguments to this command.

        """
        parser = argparse.ArgumentParser(description=self.help)
        parser.add_argument('-c', '--configpath',
                            action='store',
                            type=Path,
                            default=self.default_config_path)
        parser.add_argument('--makeconfig',
                            action='store_true',
                            help="Write configuration file to --configpath")

        parser.add_argument('-F', '--force_fore',
                            action='store',
                            type=str,
                            default="",
                            help="Make all internal output use this foreground (e.g. 'WHITE')")

        parser.add_argument('-B', '--force_back',
                            action='store',
                            type=str,
                            default="",
                            help="Make all internal output use this background (e.g. 'BLACK')")

        parser.add_argument('-V', '--version', action='version', version=self.version)
        parser.add_argument('-w', '--windows_colour', action='store_true',
                            help='force colorama into covert mode (if broken color symbols on windows)')
        parser.add_argument('-v', '--verbosity', action='store', dest='verbosity', default=1,
                            type=int, choices=[0, 1, 2, 3],
                            help='Verbosity level; 0=minimal output, 1=normal output,'
                                 '2=verbose output, 3=very verbose output')
        # parser.add_argument('--traceback', action='store_true',
        #                     help='Raise on CommandError exceptions')
        parser.add_argument('--no-color', action='store_true', dest='no_color', default=False,
                            help="Don't colorize the command output.")

        parser.add_argument('--list-dicts', action='store_true', dest='list_dicts',
                            help='show index of products mapped to row numbers of templates')

        parser.add_argument('--open-config', action='store', dest='open_config',
                            nargs='?', const='vi', default=False,
                            help="Open config file with editor, editor command"
                                 " as argument or vi as default")

        # add table paths args NOTE load descriptions
        for key, val in self.config['dirs'].items():
            parser.add_argument(f'-{key[0]}', f'--{key}', action='store', default=val, type=str)
        for key, val in self.config['dictionary_sheets'].items():
            parser.add_argument(f'--{key}', action='store', default=val, type=str)

        return parser.parse_args()

    def make_config(self, path=None):
        """ write a config file """
        path = self.get_config_path(path)

        if path.exists():
            ans = ''
            while ans not in ("y", "n", "0"):
                ans = input(f"File '{path}' exists, overwrite? (y/n/0): ").lower()
            if ans != "y":
                self.vprint("Aborting...", v=0, colour='YELLOW')
                sys.exit(4)

        for key in self.config['dirs'].keys():
            val = getattr(self.args, key)
            if not val:
                val = input(f"'{key}' is not in arguments, please type it in: ")
            self.config['dirs'][key] = val
        self.vprint(f"= Writing config to '{path}'", v=2, colour='BLUE')
        path.parent.mkdir(parents=True, exist_ok=True)  # make config path
        with open(path, "wb") as f:
            f.write(yaml.dump(self.config, allow_unicode=True, encoding='utf-8'))

    def read_config(self, path=None):
        """ read configuration """
        path = self.get_config_path(path)

        config = None
        if path.exists():
            with open(path, encoding='utf8') as f:
                config = yaml.load(f.read(), Loader=yaml.SafeLoader)
            if self.extra_verborse():
                self.vprint(f"* Loaded config from file '{path}'", colour='BLUE')
                pprint.pprint(config)
        else:
            self.vprint(f"Couldn't find config file in '{path}',\n"
                        "make sure all arguments are provided in command line!",
                        v=1, colour='YELLOW')

        # update config if needed
        conf_ver = config.get('config_version', 0)
        if conf_ver < CONFIG_VERSION:
            self.vprint(f"Config version {conf_ver} is outdated, performing update, please confirm"
                        " changes yourself!")
            config.pop('output_header_babeldate_format', None)  # remove old key
            config["output_header_date_format"] = "%d MMMM %Y г."
            config["constants_product_name_col"] = "S"
            config["filename_format"] = "{product_name} {key.form} {key.manufacturer} %d.%m.%y.xlsx"
            config['config_version'] = CONFIG_VERSION
            with open(path, "wb") as f:
                f.write(yaml.dump(config, allow_unicode=True, encoding='utf-8'))
        return config

    def vprint(self, text, v=1, end='\n', colour=None, log=0, out=sys.stdout):
        if self.args.verbosity >= v:
            if self.args.windows_colour:
                out = AnsiToWin32(out).stream
                default_style = Style.BRIGHT
            else:
                default_style = ""
            if self.args.force_fore:
                default_style = getattr(Fore, self.args.force_fore.upper(), 'WHITE')
            if self.args.force_back:
                default_style += getattr(Back, self.args.force_back.upper(), 'BLACK')
            if colour:
                f = getattr(Fore, colour.upper(), 'WHITE')
                text = f"{f}{text}{Style.RESET_ALL}"
            out.write(f"{default_style}{text}{end}")
            out.flush()
        """
        if log >= v:
            with open(self.args.logfile, 'a') as f:
                f.write(text+'\n')
        """

    def execute(self):
        self.args = self.run_argparse()
        colorama_init(convert=self.args.windows_colour)
        start_text = self.start_text.format(self, Fore, Style, python_version())
        self.vprint(start_text, v=1)

        if self.args.open_config:
            self.open_config()
            sys.exit(0)

    def get_config_path(self, path=None):
        return Path(path) if path else self.args.configpath

    def open_config(self):
        run([self.args.open_config, str(self.get_config_path())])
